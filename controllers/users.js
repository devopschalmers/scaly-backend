var express = require('express');
var User = require('../models/user');

var router = express.Router();

// Return all users
router.get('/api/users', function(req, res, next) {
    User.find(function(err, users) {
        if (err) { return next(err); }
        res.json({'users': users});
    });
});

// Return specific user by ID
router.get('/api/users/:id', function(req, res, next) {
    var id = req.params.id;
    User.findById(id, function(err, user) {
        if (err) { return next(err); }
        if (user === null) {
            return res.status(404).json({'message': 'User with id ${id} not found'});
        }
        res.json(user);
    });
});

// Delete all users
router.delete('/api/users', function(req, res, next) {
    User.deleteMany({})
        .catch(function(error){
            console.log(error);
            return res.status(500).json({'message': 'Error while clearing database: '+error});
    });
    return res.status(200).json({'message': 'ok'});
});

// Delete an user given an ID
router.delete('/api/users/:id', function(req, res, next) {
    var id = req.params.id;
    User.findOneAndDelete({_id: id}, function(err, user) {
        if (err) { return next(err); }
        if (user === null) {
            return res.status(404).json({'message': 'user not found'});
        }
        res.json(user);
    });
});

// Add a new user
router.post('/api/users', function(req, res, next) {
    var newuser = new User(req.body);
    newuser.save(function (error) {
        if (error) {
            console.log('Error storing object: '+error);
            return res.status(400).json({'message': 'Error storing object: '+error});
        }
    });
    return res.status(201).json(newuser);
});

// Update an user given an ID
router.patch('/api/users/:id', function(req, res, next) {
	var id = req.params.id;
	User.findById(id, function(err, user){
		if(err) { return next(err); }
        if(user == null) {
            return res.status(404).json({'message': 'user with id ${id} not found'});
        }
        user.userRef = (req.body.userRef || user.userRef);
        user.totalPrice = (req.body.totalPrice || user.totalPrice);
        user.userStatus = (req.body.userStatus || user.userStatus);
        user.productsList = (req.body.productsList || user.productsList);
        user.save();
        res.json(user);
	});
});

module.exports = router;

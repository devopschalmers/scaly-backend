var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name : { type: String },
    lastName : { type: String },
    username : { type: String },
    identification : { type: String },
    age: { type: Number}
});


module.exports = mongoose.model('users', userSchema);

